# advent of code 2020

My solutions for the [Advent of Code](https://adventofcode.com/) 2020. Most of these are in Haskell, since I was learning it in uni and I felt like practicing.